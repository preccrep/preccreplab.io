---
layout: post
title: PAT 1060 Are They Equal
date: '2022-07-19 10:04:01 +0800'
categories: [刷题, PAT]
tags: [题解]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805413520719872)

题目思路不难, 但是不好拿满分.

AC 代码:

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
using namespace std;
int n;
string a,b,s1,s2;
string toStandard(string s){
    int pos=s.find(".");
    if(pos==s.npos) pos=s.size();
    string left=s.substr(0,pos),right="";
    if(pos<s.size()) right=s.substr(pos+1);
    int i=0;
    while(i<left.size()&&left[i]=='0') i++;
    if(i==left.size()) left="0";
    else left=left.substr(i);
    if(right.size()>0){
        i=right.size()-1;
        while(i>=0&&right[i]=='0') i--;
        if(i<0) right="0";
        else right=right.substr(0,i+1);
    }
    // cout<<s<<" "<<left<<" "<<right<<endl;
    int len=0;
    if(left!="0") len=left.size();
    else if(right.size()>0&&right!="0"){
        i=0;
        while(i<right.size()&&right[i]=='0') i++;
        if(i<right.size()){
            len=-i;
        }
    }
    string ans="0.";
    if(len<0){
        string tmp=right.substr(i);
        if(tmp.size()<n){
            ans+=tmp;
            for(int j=1;j<=n-tmp.size();++j)
                ans+="0";
        }else ans+=tmp.substr(0,n);
    }else if(len==0){
        if(right!="0"){
            if(right.size()<n){
                ans+=right;
                for(int j=1;j<=n-right.size();++j)
                    ans+="0";
            }else ans+=right.substr(0,n);
        }else{
            for(int j=1;j<=n;++j)
                ans+="0";
        }
    }else{
        string tmp=left+right;
        if(tmp.size()<n){
            ans+=tmp;
            for(int j=1;j<=n-tmp.size();++j)
                ans+="0";
        }else ans+=tmp.substr(0,n);
    }
    ans+="*10^";
    ans+=to_string(len);
    return ans;
}
int main() {
    cin>>n>>a>>b;
    if(n==0){
        cout<<"YES 0*10^0"<<endl;
        return 0;
    }
    s1=toStandard(a),s2=toStandard(b);
    if(s1==s2) cout<<"YES "<<s1<<endl;
    else cout<<"NO "<<s1<<" "<<s2<<endl;
    return 0;
}
```

总结出了以下的测试用例, 全过了就一定可以满分.

Input Specification:

```
3 12300 12358.9
1 12300 12358.9
1 1.2300 1.23589
5 1.2300 1.23589
4 0.01234 0.012345
5 0.01234 0.012345
5 0.1234 0.12345
0 0.11 0
1 0.1 0
1 0.0 0.1
1 0.0 0.000
1 00.0 0.000
4 00.0 0.000
5 00.0 0.000
1 05.0 5.000
1 00.01 0.010
```

Output Specification:

```
YES 0.123*10^5
YES 0.1*10^5
YES 0.1*10^1
NO 0.12300*10^1 0.12358*10^1
YES 0.1234*10^-1
NO 0.12340*10^-1 0.12345*10^-1
NO 0.12340*10^0 0.12345*10^0
YES 0.*10^0或者YES 0.0*10^0，都可以AC，测试点应该没有这个例子
NO 0.1*10^0 0.0*10^0
NO 0.0*10^0 0.1*10^0
YES 0.0*10^0
YES 0.0*10^0
YES 0.0000*10^0
YES 0.00000*10^0
YES 0.5*10^1
YES 0.1*10^-1
```


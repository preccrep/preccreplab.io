---
layout: post
title: PAT 1134 Vertex Cover
date: '2022-08-02 14:23:17 +0800'
categories: [刷题, PAT]
tags: [题解, 图论]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805346428633088)

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
#include <cmath>
using namespace std;
int n,m,k;
bool v[10010];
vector<int>vertex[10010];
int main() {
    scanf("%d%d",&n,&m);
    for(int i=1;i<=m;++i){
        int v1,v2;
        scanf("%d%d",&v1,&v2);
        vertex[v1].push_back(i);
        vertex[v2].push_back(i);
    }
    scanf("%d",&k);
    while(k--){
        int num,u;
        scanf("%d",&num);
        memset(v,false,sizeof(v));
        for(int i=1;i<=num;++i){
            scanf("%d",&u);
            for(int j=0;j<vertex[u].size();++j)
                v[vertex[u][j]]=true;
        }
        bool flag=true;
        for(int i=1;i<=m;++i)
            if(!v[i]){
                flag=false;
                break;
            }
        if(flag) printf("Yes\n");
        else printf("No\n");
    }
    return 0;
}
```

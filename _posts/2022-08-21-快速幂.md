---
layout: post
title: 快速幂
date: '2022-08-21 12:30:40 +0800'
categories: [刷题, AcWing, 文章]
tags: [algorithm, notes]
---

## AcWing 90 64位整数乘法

[Link](https://www.acwing.com/problem/content/92/)

如果直接计算 a 乘 b 就会超过 long long 的最大范围, 所以采用快速幂的思想把 b 写成二进制形式, 如果第 i 位上为 1 就在结果上加 `a*(1<<i)`, 并且每次加上之后取模就可以了.

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
using namespace std;
typedef long long ll;
ll a,b,p,ans;
int main() {
    scanf("%lld%lld%lld",&a,&b,&p);
    while(b){
        if(b&1) ans=(ans+a)%p;
        b>>=1;
        a=(a<<1)%p;
    }
    printf("%lld\n",ans);
    return 0;
}
```



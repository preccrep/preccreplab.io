---
layout: post
title: PAT 1066 Root of AVL Tree
date: '2022-08-01 13:54:12 +0800'
categories: [刷题, PAT]
tags: [题解, 树, AVL]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805404939173888)

AVL 模板题, 我之前也写过一篇相关的[题解](https://www.cnblogs.com/preccrep/p/16398174.html#root-of-avl-tree-25), 有图, 较为详细.

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
using namespace std;
int n,v;
struct node{
    int val;
    struct node *lch,*rch;
};
int getHeight(node *root){
    if(root==NULL) return 0;
    return max(getHeight(root->lch),getHeight(root->rch))+1;
}
node *leftRotate(node *root){
    if(root->rch==NULL) return NULL;
    node *p=root->rch;
    root->rch=p->lch;
    p->lch=root;
    return p;
}
node *rightRotate(node *root){
    if(root->lch==NULL) return NULL;
    node *p=root->lch;
    root->lch=p->rch;
    p->rch=root;
    return p;
}
node *leftRightRotate(node *root){
    if(root->lch==NULL) return NULL;
    if(root->lch->rch==NULL) return NULL;
    node *p=leftRotate(root->lch);
    root->lch=p->rch;
    p->rch=root;
    return p;
}
node *rightLeftRotate(node *root){
    if(root->rch==NULL) return NULL;
    if(root->rch->lch==NULL) return NULL;
    node *p=rightRotate(root->rch);
    root->rch=p->lch;
    p->lch=root;
    return p;
}
node *insert(node *root,int val){
    if(root==NULL){
        root=new node();
        root->val=val,root->lch=root->rch=NULL;
    }else if(val<root->val){
        root->lch=insert(root->lch,val);
        if(getHeight(root->lch)-getHeight(root->rch)==2){
            if(getHeight(root->lch->lch)-getHeight(root->lch->rch)==1) root=rightRotate(root);
            else root=leftRightRotate(root);
        }
    }else{
        root->rch=insert(root->rch,val);
        if(getHeight(root->rch)-getHeight(root->lch)==2){
            if(getHeight(root->rch->rch)-getHeight(root->rch->lch)==1) root=leftRotate(root);
            else root=rightLeftRotate(root);
        }
    }
    return root;
}
int main() {
    node *root=NULL;
    scanf("%d",&n);
    for(int i=1;i<=n;++i){
        scanf("%d",&v);
        root=insert(root,v);
    }
    printf("%d\n",root->val);
    return 0;
}
```

---
layout: post
title: PAT 1140 Look-and-say Sequence
date: '2022-08-03 16:58:26 +0800'
categories: [刷题, PAT]
tags: [题解, 字符串]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805344490864640)

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
using namespace std;
int d,n;
int main() {
    scanf("%d%d",&d,&n);
    string s=to_string(d);
    for(int i=2;i<=n;++i){
        int cnt=1,j=1;
        string ss="";
        for(;j<s.size();++j){
            if(s[j]==s[j-1])
                cnt++;
            else{
                ss+=s[j-1]+to_string(cnt);
                cnt=1;
            }
        }
        ss+=s[j-1]+to_string(cnt);
        s=ss;
    }
    cout<<s<<endl;
    return 0;
}
```

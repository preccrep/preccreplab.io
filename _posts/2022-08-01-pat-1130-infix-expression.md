---
layout: post
title: PAT 1130 Infix Expression
date: '2022-08-01 15:51:30 +0800'
categories: [刷题, PAT]
tags: [题解, 树]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805347921805312)

中序遍历输出表达式.

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
using namespace std;
int n;
struct node{
    string data;
    int left=-1,right=-1;
}tree[25];
bool v[25];
void InOrder(int root){
    if(tree[root].left!=-1){
        if(tree[tree[root].left].left!=-1||tree[tree[root].left].right!=-1){
            cout<<"(";
            InOrder(tree[root].left);
            cout<<")";
        }else InOrder(tree[root].left);
    }
    cout<<tree[root].data;
    if(tree[root].right!=-1){
        if(tree[tree[root].right].left!=-1||tree[tree[root].right].right!=-1){
            cout<<"(";
            InOrder(tree[root].right);
            cout<<")";
        }else InOrder(tree[root].right);
    }
}
int main() {
    cin>>n;
    for(int i=1;i<=n;++i){
        cin>>tree[i].data>>tree[i].left>>tree[i].right;
        if(tree[i].left!=-1) v[tree[i].left]=true;
        if(tree[i].right!=-1) v[tree[i].right]=true;
    }
    int root=0;
    for(int i=1;i<=n;++i)
        if(!v[i]){
            root=i;
            break;
        }
    InOrder(root);
    printf("\n");
    return 0;
}
```

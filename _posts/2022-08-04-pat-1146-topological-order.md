---
layout: post
title: PAT 1146 Topological Order
date: '2022-08-04 17:10:33 +0800'
categories: [刷题, PAT]
tags: [题解, 图论]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805343043829760)

拓扑排序.

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
using namespace std;
int n,m,k,a;
bool g[1010][1010],v[1010];
vector<int>in[1010],out[1010];
vector<int>ans;
int main() {
    scanf("%d%d",&n,&m);
    for(int i=1;i<=m;++i){
        int v1,v2;
        scanf("%d%d",&v1,&v2);
        g[v1][v2]=true;
    }
    for(int i=1;i<=n;++i)
        for(int j=1;j<=n;++j)
            if(i!=j&&g[i][j])
                out[i].push_back(j),in[j].push_back(i);
    scanf("%d",&k);
    for(int c=0;c<k;++c){
        memset(v,false,sizeof(v));
        bool isTopo=true;
        for(int i=1;i<=n;++i){
            scanf("%d",&a);
            if(!isTopo) continue;
            v[a]=true;
            bool flag=true;
            for(int j=0;j<in[a].size();++j)
                if(!v[in[a][j]]){
                    flag=false;
                    break;
                }
            if(!flag) isTopo=false;
        }
        if(!isTopo) ans.push_back(c);
    }
    for(int i=0;i<ans.size();++i){
        if(i!=0) printf(" ");
        printf("%d",ans[i]);
    }
    printf("\n");
    return 0;
}
```

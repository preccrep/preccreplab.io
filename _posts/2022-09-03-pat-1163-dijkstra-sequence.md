---
layout: post
title: PAT 1163 Dijkstra Sequence
date: '2022-09-03 16:24:21 +0800'
categories: [刷题, PAT]
tags: [题解, 图论, Dijkstra, 最短路径]
---

[LINK](https://pintia.cn/problem-sets/994805342720868352/problems/1478635670373253120)

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
using namespace std;
const int INF=0x3f3f3f3f;
int n,m,k;
int g[1010][1010],a[1010],d[1010];
bool v[1010];
bool Dijkstra(){
    memset(d,63,sizeof(d));
    memset(v,false,sizeof(v));
    d[a[1]]=0;
    for(int i=1;i<=n;++i){
        int u=-1,minv=INF;
        for(int j=1;j<=n;++j)
            if(!v[j]&&d[j]<minv){
                u=j;
                minv=d[j];
            }
        if(u==-1) break;
        v[u]=true;
        for(int j=1;j<=n;++j)
            if(!v[j]&&g[u][j]!=INF&&d[j]>d[u]+g[u][j]){
                d[j]=d[u]+g[u][j];
            }
    }
    for(int i=2;i<=n;++i)
        if(d[a[i]]<d[a[i-1]])
            return false;
    return true;
}
int main() {
    memset(g,INF,sizeof(g));
    scanf("%d%d",&n,&m);
    for(int i=1;i<=m;++i){
        int s,t;
        scanf("%d%d",&s,&t);
        scanf("%d",&g[s][t]);
        g[t][s]=g[s][t];
    }
    scanf("%d",&k);
    while(k--){
        for(int i=1;i<=n;++i)
            scanf("%d",&a[i]);
        printf("%s\n",Dijkstra()?"Yes":"No");
    }
    return 0;
}
```

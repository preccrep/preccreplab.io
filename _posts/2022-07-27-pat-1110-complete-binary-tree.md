---
layout: post
title: PAT 1110 Complete Binary Tree
date: '2022-07-27 14:55:20 +0800'
categories: [刷题, PAT]
tags: [题解, DFS, 树]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805359372255232)

怎么说呢, 竟然会卡在“读入的不一定是字符从而应该用字符串接收”上. 因为, 可以是多位数啊...

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
#include <queue>
using namespace std;
int n,root,maxn,last;
struct node{
    int left=-1,right=-1;
}tree[25];
bool v[25];
void DFS(int x,int index){
    if(index>maxn){
        maxn=index;
        last=x;
    }
    if(tree[x].left!=-1) DFS(tree[x].left,index<<1);
    if(tree[x].right!=-1) DFS(tree[x].right,(index<<1)+1);
}
int main() {
    cin>>n;
    for(int i=0;i<n;++i){
        string a,b;
        cin>>a>>b;
        if(a!="-"){
            tree[i].left=stoi(a);
            v[stoi(a)]=true;
        }
        if(b!="-"){
            tree[i].right=stoi(b);
            v[stoi(b)]=true;
        }
    }
    for(int i=0;i<n;++i)
        if(!v[i]){
            root=i;
            break;
        }
    DFS(root,1);
    if(maxn==n) printf("YES %d\n",last);
    else printf("NO %d\n",root);
    return 0;
}
```

---
layout: post
title: PAT 1142 Maximal Clique
date: '2022-08-03 17:27:18 +0800'
categories: [刷题, PAT]
tags: [题解, 图论]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805343979159552)

题目很简单, 虽然是最大团, 但只是让你判断, 没让你求... 所以暴力就可以做.

不过写这篇题解其实是想写一写最大团相关算法的说 (跟本题没多大关系 QAQ)

## 这道题

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
using namespace std;
int Nv,Ne,m,k;
bool g[210][210],v[210];
int vertex[210];
int main() {
    scanf("%d%d",&Nv,&Ne);
    for(int i=1;i<=Ne;++i){
        int v1,v2;
        scanf("%d%d",&v1,&v2);
        g[v1][v2]=g[v2][v1]=true;
    }
    scanf("%d",&m);
    while(m--){
        memset(v,false,sizeof(v));
        scanf("%d",&k);
        for(int i=1;i<=k;++i){
            scanf("%d",&vertex[i]);
            v[vertex[i]]=true;
        }
        bool isClique=true,isMaximal=false;
        for(int i=1;i<k;++i){
            for(int j=i+1;j<=k;++j)
                if(!g[vertex[i]][vertex[j]]){
                    isClique=false;
                    break;
                }
            if(!isClique) break;
        }
        if(isClique){
            isMaximal=true;
            for(int i=1;i<=Nv;++i)
                if(!v[i]){
                    bool flag=true;
                    for(int j=1;j<=k;++j)
                        if(!g[i][vertex[j]]){
                            flag=false;
                            break;
                        }
                    if(flag){
                        isMaximal=false;
                        break;
                    }
                }
        }
        if(isMaximal) printf("Yes\n");
        else if(isClique) printf("Not Maximal\n");
        else printf("Not a Clique\n");
    }
    return 0;
}
```

## 最大团

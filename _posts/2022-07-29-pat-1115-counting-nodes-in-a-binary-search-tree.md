---
layout: post
title: PAT 1115 Counting Nodes in a Binary Search Tree
date: '2022-07-29 13:41:34 +0800'
categories: [刷题, PAT]
tags: [题解, 树, BST, DFS]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805355987451904)

练习一下链表建树 2333

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
using namespace std;
#define INF 1010
int n,a,maxv,n1,n2;
struct node{
    int val;
    struct node *left,*right;
}tree[1010];
int num[1010];
node* Insert(node *root,int i){
    if(root==NULL){
        root=new node();
        root->val=i;
        root->left=root->right=NULL;
    }else if(i<=root->val) root->left=Insert(root->left,i); //是root->left=Insert(...)不是root
    else root->right=Insert(root->right,i);
    return root;
}
void DFS(node *root,int depth){
    if(root==NULL){
        maxv=max(maxv,depth-1);
        return;
    }
    num[depth]++;
    DFS(root->left,depth+1);
    // cout<<root->val<<endl;
    DFS(root->right,depth+1);
}
int main() {
    scanf("%d",&n);
    node *root=NULL;
    for(int i=1;i<=n;++i){
        scanf("%d",&a);
        root=Insert(root,a);
    }
    DFS(root,0);
    printf("%d + %d = %d\n",num[maxv],num[maxv-1],num[maxv]+num[maxv-1]);
    return 0;
}
```

---
layout: post
title: PAT 1125 Chain the Ropes
date: '2022-07-31 16:44:29 +0800'
categories: [刷题, PAT]
tags: [题解, 贪心]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805350316752896)

越早加入绳子的段, 对折的次数就越多, 因此想让绳子最长就应该让长度较长的段对折次数尽可能少.

将所有段从小到大排序, 依次加入绳子中即可.

AC 代码:

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
using namespace std;
int n,a[10010],cur;
int main() {
    scanf("%d",&n);
    for(int i=1;i<=n;++i)
        scanf("%d",&a[i]);
    sort(a+1,a+1+n);
    cur=a[1];
    for(int i=2;i<=n;++i)
        cur=(cur+a[i])>>1;
    printf("%d\n",cur);
    return 0;
}
```

---
layout: post
title: PAT 1057 Stack
date: '2022-08-13 22:41:22 +0800'
categories: [刷题, PAT]
tags: [题解, 树状数组, 二分]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805417945710592)

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
#include <stack>
using namespace std;
#define lowbit(i) ((i)&(-i))
const int maxn=100010;
int n,a,tree[maxn];
stack<int>st;
void update(int x,int v){
    for(int i=x;i<maxn;i+=lowbit(i))
        tree[i]+=v;
}
int getsum(int x){
    int sum=0;
    for(int i=x;i>=1;i-=lowbit(i))
        sum+=tree[i];
    return sum;
}
void PeekMedian(){
    int target=(st.size()+1)>>1;
    int low=1,high=maxn;
    while(low<high){
        int mid=(low+high)>>1;
        if(getsum(mid)>=target)
            high=mid;
        else low=mid+1;
    }
    printf("%d\n",low);
}
int main() {
    scanf("%d",&n);
    while(n--){
        char s[15];
        scanf("%s",s);
        if(s[1]=='u'){
            scanf("%d",&a);
            update(a,1);
            st.push(a);
        }else if(s[1]=='o'){
            if(st.empty()){
                printf("Invalid\n");
                continue;
            }
            a=st.top();
            st.pop();
            update(a,-1);
            printf("%d\n",a);
        }else{
            if(st.empty()){
                printf("Invalid\n");
                continue;
            }
            PeekMedian();
        }
    }
    return 0;
}
```

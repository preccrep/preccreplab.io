---
layout: post
title: PAT 1107 Social Clusters
date: '2022-07-26 15:07:34 +0800'
categories: [刷题, PAT]
tags: [题解, DFS]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805361586847744)

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
#include <cmath>
using namespace std;
int n;
vector<int>hobbies[1010],people[1010];
vector<int>ans;
bool v[1010],vp[1010];
void DFS(int x,int &num){
    v[x]=true;
    for(int i=0;i<hobbies[x].size();++i)
        if(!vp[hobbies[x][i]]){
            num++; //num加的是人数而不是hobby数,还要注意是否重复访问,所以要在这里加
            vp[hobbies[x][i]]=true;
            for(int j=0;j<people[hobbies[x][i]].size();++j)
                if(!v[people[hobbies[x][i]][j]])
                    DFS(people[hobbies[x][i]][j],num);
        }
}
int main() {
    scanf("%d",&n);
    for(int i=1;i<=n;++i){
        int k,h;
        scanf("%d:",&k);
        for(int j=1;j<=k;++j){
            scanf("%d",&h);
            people[i].push_back(h);
            hobbies[h].push_back(i);
        }
    }
    for(int i=1;i<=1000;++i)
        if(!v[i]&&hobbies[i].size()){
            int num=0;
            DFS(i,num);
            ans.push_back(num);
        }
    sort(ans.begin(),ans.end(),greater<int>());
    printf("%d\n",ans.size());
    for(int i=0;i<ans.size();++i){
        if(i!=0) printf(" ");
        printf("%d",ans[i]);
    }
    printf("\n");
    return 0;
}
```

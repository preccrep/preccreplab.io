---
layout: post
title: PAT 1129 Recommendation System
date: '2022-08-01 15:27:23 +0800'
categories: [刷题, PAT]
tags: [题解, STL]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805348471259136)

`set` 原来还可以这么用 ( )

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
#include <set>
using namespace std;
int n,k,a,items[50010];
struct node{
    int id,num;
    // node(int _id,int _num): id(_id), num(_num){}
    bool operator<(const node &x)const{
        return num!=x.num?num>x.num:id<x.id;
    }
};
set<node>st;
int main() {
    scanf("%d%d",&n,&k);
    for(int i=1;i<=n;++i){
        scanf("%d",&a);
        if(i>1){
            printf("%d:",a);
            int cnt=0;
            for(auto it=st.begin();cnt<k&&it!=st.end();++it){
                printf(" %d",it->id);
                cnt++;
            }
            printf("\n");
        }
        auto it=st.find(node{a,items[a]});
        if(it!=st.end()) st.erase(it);
        items[a]++;
        st.insert(node{a,items[a]});
    }
    return 0;
}
```

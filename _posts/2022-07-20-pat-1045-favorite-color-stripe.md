---
layout: post
title: PAT 1045 Favorite Color Stripe
date: '2022-07-20 11:43:48 +0800'
categories: [刷题, PAT]
tags: [题解, dp, 子序列]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805437411475456)

本质上是最长上升子序列的问题, 只是这里的“上升”是按题目给出的 order 来比较的.

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
using namespace std;
int n,m,l,x,maxv;
int f[210],stripe[10010],dp[10010];
int main() {
    scanf("%d%d",&n,&m);
    for(int i=1;i<=m;++i){
        scanf("%d",&x);
        f[x]=i;
    }
    scanf("%d",&l);
    for(int i=1;i<=l;++i)
        scanf("%d",&stripe[i]);
    for(int i=1;i<=l;++i){
        if(!f[stripe[i]]) continue;
        dp[i]=1;
        for(int j=1;j<i;++j)
            if(f[stripe[j]]&&f[stripe[j]]<=f[stripe[i]])
                dp[i]=max(dp[i],dp[j]+1);
        maxv=max(maxv,dp[i]);
    }
    printf("%d\n",maxv);
    return 0;
}
```

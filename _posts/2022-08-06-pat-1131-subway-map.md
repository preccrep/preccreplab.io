---
layout: post
title: PAT 1131 Subway Map
date: '2022-08-06 13:08:47 +0800'
categories: [刷题, PAT]
tags: [题解, 图论, DFS]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805347523346432)

个人认为关键思路在于 `line` 的确定. 我是想到两点确定一条直线, 所以用 `line[i][j]` 表示 `i` 和 `j` 同在的那条线路的编号.

接下来就比较模式化了, 用 DFS 找最优解, 有点麻烦 (但也不足以为难) 的是, 需要记录 DFS 的过程 (我记录在了 `tempPath` 中), 然后通过各种比较得出 `minPath`, 这和之前用 Dijkstra 求最短路并记录路径的过程非常相似.

AC 代码:

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
using namespace std;
int n,m,x,cur,curline,k,s,t,minNum,minStops;
bool v[10010];
int line[10010][10010];
vector<int>g[10010];
vector<int>minPath,tempPath;
void DFS(int x,int num,int stops){
    if(x==t){
        if(stops<minStops){
            minStops=stops;
            minNum=num;
            minPath=tempPath;
        }else if(stops==minStops&&num<minNum){
            minNum=num;
            minPath=tempPath;
        }
        return;
    }
    for(int i=0;i<g[x].size();++i)
        if(!v[g[x][i]]){
            v[g[x][i]]=true;
            tempPath.push_back(g[x][i]);
            int templine=curline,curnum=num;
            if(curline!=line[x][g[x][i]]) curnum++;
            curline=line[x][g[x][i]];
            DFS(g[x][i],curnum,stops+1);
            curline=templine;
            v[g[x][i]]=false;
            tempPath.pop_back();
        }
}
int main() {
    scanf("%d",&n);
    for(int i=1;i<=n;++i){
        scanf("%d",&m);
        for(int j=1;j<=m;++j){
            scanf("%d",&x);
            if(j==1) cur=x;
            else{
                line[cur][x]=line[x][cur]=i;
                g[x].push_back(cur);
                g[cur].push_back(x);
                cur=x;
            }
        }
    }
    scanf("%d",&k);
    while(k--){
        memset(v,false,sizeof(v));
        minNum=minStops=0x3f3f3f3f;
        tempPath.clear();
        minPath.clear();
        scanf("%d%d",&s,&t);
        v[s]=true;
        tempPath.push_back(s);
        DFS(s,0,0);
        printf("%d\n",minStops);
        cur=s,curline=line[s][minPath[1]];
        for(int i=1;i<minPath.size();++i){
            if(line[minPath[i-1]][minPath[i]]!=curline){
                printf("Take Line#%d from %04d to %04d.\n",curline,cur,minPath[i-1]);
                cur=minPath[i-1];
                curline=line[minPath[i-1]][minPath[i]];
            }
        }
        printf("Take Line#%d from %04d to %04d.\n",curline,cur,t);
    }
    return 0;
}
```

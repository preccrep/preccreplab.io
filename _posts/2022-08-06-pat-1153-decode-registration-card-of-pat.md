---
layout: post
title: PAT 1153 Decode Registration Card of PAT
date: '2022-08-06 10:29:48 +0800'
categories: [刷题, PAT]
tags: [题解, 字符串]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/1071785190929788928)

思路不难, 但是很难做对.

必须用 `printf` 输出, 否则测试点 3 会超时. 要把 `string` 类型通过 `s.c_str()` 转换成 C 字符串.

可以在输入了 type 之后进行计算.

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
#include <unordered_map>
using namespace std;
int n,m,Type;
string Term;
struct node{
    string s;
    int val;
}testee[10010];
bool cmp(const node& a,const node& b){
    if(a.val!=b.val) return a.val>b.val;
    return a.s<b.s;
}
int main() {
    cin>>n>>m;
    int index=0;
    for(int i=1;i<=n;++i)
        cin>>testee[i].s>>testee[i].val;
    for(int i=1;i<=m;++i){
        cin>>Type>>Term;
        printf("Case %d: %d %s\n",i,Type,Term.c_str());
        vector<node>ans;
        int num=0,tot=0;
        if(Type==1){
            for(int j=1;j<=n;++j)
                if(testee[j].s[0]==Term[0]) //比较的是字符,虽然Term长度是1但也要取Term[0]
                    ans.push_back(testee[j]);
        }else if(Type==2){
            for(int j=1;j<=n;++j)
                if(testee[j].s.substr(1,3)==Term)
                    num++,tot+=testee[j].val;
            if(num) printf("%d %d\n",num,tot);
        }else{
            unordered_map<string,int>temp;
            for(int j=1;j<=n;++j)
                if(testee[j].s.substr(4,6)==Term)
                    temp[testee[j].s.substr(1,3)]++;
            for(auto it:temp) ans.push_back({it.first,it.second});
        }
        sort(ans.begin(),ans.end(),cmp);
        for(int j=0;j<ans.size();++j)
            printf("%s %d\n",ans[j].s.c_str(),ans[j].val);
        if(((Type==1||Type==3)&&ans.size()==0)||(Type==2&&num==0)) cout<<"NA"<<endl;
    }
    return 0;
}
```

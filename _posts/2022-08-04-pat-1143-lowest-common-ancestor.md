---
layout: post
title: PAT 1143 Lowest Common Ancestor
date: '2022-08-04 14:07:42 +0800'
categories: [刷题, PAT]
tags: [题解, 树]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805343727501312)

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
#include <unordered_map>
using namespace std;
#define N 10010
int m,n;
unordered_map<int,int>keymap;
int pre[10010],father[10010];
bool v[10010];
void build(int left,int right){
    if(left>=right) return;
    int i=left+1;
    for(;i<=right;++i)
        if(pre[i]>=pre[left])
            break;
    if(i<=right){
        father[keymap[pre[left+1]]]=keymap[pre[left]];
        father[keymap[pre[i]]]=keymap[pre[left]];
        build(left+1,i-1);
        build(i,right);
    }else{
        father[keymap[pre[left+1]]]=keymap[pre[left]];
        build(left+1,right);
    }
}
void LCA(int v1,int v2){
    memset(v,false,sizeof(v));
    int u1=v1,u2=v2;
    while(v1){
        v[v1]=true;
        v1=father[v1];
    }
    while(v2){
        if(v[v2]) break;
        v2=father[v2];
    }
    if(v2==u1) printf("%d is an ancestor of %d.\n",pre[u1],pre[u2]);
    else if(v2==u2) printf("%d is an ancestor of %d.\n",pre[u2],pre[u1]);
    else  printf("LCA of %d and %d is %d.\n",pre[u1],pre[u2],pre[v2]);
}
int main() {
    scanf("%d%d",&m,&n);
    for(int i=1;i<=n;++i){
        scanf("%d",&pre[i]);
        keymap[pre[i]]=i;
    }
    build(1,n);
    while(m--){
        int v1,v2;
        scanf("%d%d",&v1,&v2);
        bool f1=true,f2=true;
        if(keymap.count(v1)==0) f1=false;
        if(keymap.count(v2)==0) f2=false;
        if(!f1&&f2) printf("ERROR: %d is not found.\n",v1);
        else if(f1&&!f2) printf("ERROR: %d is not found.\n",v2);
        else if(!f1&&!f2) printf("ERROR: %d and %d are not found.\n",v1,v2);
        else LCA(keymap[v1],keymap[v2]);
    }
    return 0;
}
```

---
layout: post
title: PAT 1149 Dangerous Goods Packaging
date: '2022-08-05 13:04:52 +0800'
categories: [刷题, PAT]
tags: [题解, 图论]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/1038429908921778176)

1. 不要用 `bool g[100010][100010]`, 会爆内存.
2. 要用 `unordered_map` 存储 (即, 用邻接表而不是邻接矩阵!), 判断要 ship 的货物是否在 incompatible list 中.

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
#include <unordered_map>
using namespace std;
int n,m,k,num;
int good[100010];
bool v[100010];
unordered_map<int,vector<int>>incompatible;
int main() {
    scanf("%d%d",&n,&m);
    for(int i=1;i<=n;++i){
        int v1,v2;
        scanf("%d%d",&v1,&v2);
        incompatible[v1].push_back(v2);
        incompatible[v2].push_back(v1);
    }
    while(m--){
        memset(v,0,sizeof(v));
        scanf("%d",&k);
        for(int i=1;i<=k;++i){
            scanf("%d",&good[i]);
            v[good[i]]=true;
        }
        bool flag=true;
        for(int i=1;i<=k;++i){
            for(int j=0;j<incompatible[good[i]].size();++j)
                if(v[incompatible[good[i]][j]]){
                    flag=false;
                    break;
                }
            if(!flag) break;
        }
        if(!flag) printf("No\n");
        else printf("Yes\n");
    }
    return 0;
}
```

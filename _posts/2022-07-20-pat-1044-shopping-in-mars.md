---
layout: post
title: PAT 1044 Shopping in Mars
date: '2022-07-20 12:16:25 +0800'
categories: [刷题, PAT]
tags: [题解, 前缀和, 子序列]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805439202443264)

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
using namespace std;
int n,m,cur;
int d[100010];
vector<pair<int,int>>ans;
int main() {
    scanf("%d%d",&n,&m);
    for(int i=1;i<=n;++i){
        scanf("%d",&d[i]);
        d[i]+=d[i-1];
    }
    cur=1000000000;
    bool flag=false;
    int i=1,j=1;
    while(i<=j&&j<=n){
        int temp=d[j]-d[i-1];
        if(temp==m){
            if(!flag){
                flag=true;
                ans.clear();
            }
            ans.push_back({i,j});
            i++,j++;
        }else if(temp<m) j++;
        else{
            if(!flag){
                if(cur>temp-m){
                    cur=temp-m;
                    ans.clear();
                    ans.push_back({i,j});
                }else if(cur==temp-m)
                    ans.push_back({i,j});
            }
            //注意这里需要判断, 不能只 i++, 否则当 i==j 时 i++ 就会导致 i>j 从而退出循环, 后面的序列就没有被遍历到.
            if(i<j) i++;
            else i++,j++;
        }
    }
    for(i=0;i<ans.size();++i)
        printf("%d-%d\n",ans[i].first,ans[i].second);
    return 0;
}
```

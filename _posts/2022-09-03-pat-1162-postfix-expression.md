---
layout: post
title: PAT 1162 Postfix Expression
date: '2022-09-03 17:12:59 +0800'
categories: [刷题, PAT]
tags: [题解, 树, 数据结构]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/1478635599577522176)

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
using namespace std;
int n;
bool v[25];
struct node{
    char data[15];
    int left,right;
}a[25];
void PostOrder(int x){
    if(a[x].left==-1){
        printf("%s",a[x].data);
        if(a[x].right!=-1){
            printf("(");
            PostOrder(a[x].right);
            printf(")");
        }
    }else{
        printf("(");
        PostOrder(a[x].left);
        printf(")");
        if(a[x].right!=-1){
            printf("(");
            PostOrder(a[x].right);
            printf(")");
        }
        printf("%s",a[x].data);
    }
}
int main() {
    scanf("%d",&n);
    for(int i=1;i<=n;++i){
        scanf("%s%d%d",a[i].data,&a[i].left,&a[i].right);
        if(a[i].left!=-1) v[a[i].left]=true;
        if(a[i].right!=-1) v[a[i].right]=true;
    }
    int root=0;
    for(int i=1;i<=n;++i)
        if(!v[i]){
            root=i;
            break;
        }
    printf("(");
    PostOrder(root);
    printf(")");
    return 0;
}
```

---
layout: post
title: Campus Network Auto Login
date: '2022-09-02 11:11:44 +0800'
categories: [技术, 网络]
tags: [个人项目]
---

I'm currently in TJU so I use `srun` as an example.

- Just use the IP address to open the login page of your campus network as usual, in a browser.

- Open the console panel (you're supposed to open it before you successfully logged in).

- Then you are redirected to the 'Successfully Login' page as you have entered your username and password correctly.

Click 'Network' and checked 'Preserve Log'. Then you can see these javascript files by filtering 'JS'.

![js.png](https://s2.loli.net/2022/09/02/mPHY5b4sSi2JyUt.png)

The functions we will use are in `all.min.js` and `jquery.srun.portal.js`.

The various encryption algorithm (md5, base64, etc.) are implemented in `all.min.js` while the logic of how to encrypt your login information are implemented in `jquery.srun.portal.js`.

Here we get the source code for jquery-base64 in `all.min.js`:

![jquery_base64.png](https://s2.loli.net/2022/09/02/Rl1Fv4qdiuAcZSb.png)

Click 'All' and find 'challenge' in the filter. We want to find the `get_challenge` property.

![challenge.png](https://s2.loli.net/2022/09/02/fdunHNqDlgZrhiW.png)

Click the name on the left and you will see details on the right, includig request url, response headers and request headers. This is where you can find the `callback` of `get_challenge`.

`srun_portal` and `rad_user_info` is just the same.

This [blog](https://blog.csdn.net/qq_41797946/article/details/89417722) is very useful, but its python implementation of `base64` has a mistake, which is tricky for a password whose length is a multiple of 3. I corrected it in my code.

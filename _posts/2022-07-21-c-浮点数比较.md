---
layout: post
title: C++ 浮点数比较
date: '2022-07-21 13:03:38 +0800'
categories: [语言, C/C++]
tags: [notes, 浮点数, 知识点]
---

```c++
const double eps=1e-8; //通常1e-8就可以了
bool equal(double a,double b)
{
    return fabs(a-b)<eps;
}
```

在 `float.h` 头文件中定义了一个常量 `FLT_EPSILON`, 可以用它作为 `eps` 的值.

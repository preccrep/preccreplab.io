---
layout: post
title: PAT 1144 The Missing Number
date: '2022-08-04 14:22:29 +0800'
categories: [刷题, PAT]
tags: [题解]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805343463260160)

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
using namespace std;
int n;
int a[100010];
int main() {
    scanf("%d",&n);
    for(int i=1;i<=n;++i)
        scanf("%d",&a[i]);
    sort(a+1,a+1+n);
    int j=1,ans=1;
    while(j<=n&&a[j]<=0) j++;
    if(j<=n){
        for(;j<=n;++j)
            if(a[j]-max(0,a[j-1])>1){ //注意这里,a[j-1]可能是负数,而比较的应该是非负数
                ans=max(0,a[j-1])+1;
                break;
            }
        if(j>n) ans=a[n]+1;
    }
    printf("%d\n",ans);
    return 0;
}
```

---
layout: post
title: VScode Word Wrap Not Working
date: '2022-07-21 12:08:37 +0800'
categories: [Utilities]
tags: [vscode]
---

想让 VScode 自动换行. 点开 Settings 搜索 word wrap 然后设置 `Editor: Word Wrap` 为 `on`, 但是什么反应都没有!

突然想起来昨天使用 Grammarly 检查英语语法错误时, VScode 曾弹出一个 Notification:

> Are you using a screen reader to operate VS Code? (Certain features like word wrap are disabled when using a screen reader)
{: .prompt-warning }

啊 没错 -.- Grammarly 是要获取屏幕读取权限的. 就是这里出了问题!

再次打开 Settings, 设置 `"editor.accessibilitySupport": "auto"` (要设置为 auto).

**Solved!**

[官方文档](https://code.visualstudio.com/docs/editor/accessibility#_screen-reader-mode)

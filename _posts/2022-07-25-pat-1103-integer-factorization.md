---
layout: post
title: PAT 1103 Integer Factorization
date: '2022-07-25 10:29:09 +0800'
categories: [刷题, PAT]
tags: [题解, 数学, DFS]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805364711604224)

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
#include <cmath>
using namespace std;
int n,k,p,m,maxv;
int power[21];
vector<int>ans,temp;
void DFS(int sum,int index,int cur){
    if(temp.size()==k){
        if(sum==0&&cur>maxv){
            ans=temp;
            maxv=cur;
        }
        return;
    }
    for(int i=index;i>=1;--i){
        if(sum>=power[i]){
            temp.push_back(i);
            DFS(sum-power[i],i,cur+i);
            temp.pop_back();
        }
    }
}
int main() {
    scanf("%d%d%d",&n,&k,&p);
    m=20;
    for(int i=1;i<=20;++i){
        power[i]=pow(i,p);
        if(power[i]>n){
            m=i-1;
            break;
        }
    }
    DFS(n,m,0);
    if(ans.size()==0) printf("Impossible\n");
    else{
        printf("%d = ",n);
        for(int i=0;i<k;++i){
            if(i!=0) printf(" + ");
            printf("%d^%d",ans[i],p);
        }
        printf("\n");
    }
    return 0;
}
```

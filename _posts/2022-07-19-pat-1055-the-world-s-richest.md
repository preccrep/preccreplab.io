---
layout: post
title: PAT 1055 The World's Richest
date: '2022-07-19 16:59:00 +0800'
categories: [刷题, PAT]
tags: [题解]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805421066272768)

亲测用 `cin + string` 会有一个测试用例超时, 用 `scanf + char[]` 可以 AC.

思路其实很直接, 但是第一眼看上去以为是区间查询.
可以重做一遍...

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
using namespace std;
int n,k,m,Amin,Amax;
struct node{
    char name[10];
    int age,netWorth;
    bool operator<(node x)const{
        if(netWorth!=x.netWorth) return netWorth>x.netWorth;
        if(age!=x.age) return age<x.age;
        return strcmp(name,x.name)<0;
    }
}person[100010];
int ages[210];
vector<node>v;
int main() {
    scanf("%d%d",&n,&k);
    for(int i=1;i<=n;++i)
        scanf("%s%d%d",person[i].name,&person[i].age,&person[i].netWorth);
    sort(person+1,person+1+n);
    for(int i=1;i<=n;++i)
        if(ages[person[i].age]<100){
            ages[person[i].age]++;
            v.push_back(person[i]);
        }
    for(int j=1;j<=k;++j){
        scanf("%d%d%d",&m,&Amin,&Amax);
        printf("Case #%d:\n",j);
        vector<node>temp;
        for(int i=0;i<v.size();++i)
            if(v[i].age>=Amin&&v[i].age<=Amax)
                temp.push_back(v[i]);
        if(temp.size()==0){
            printf("None\n");
            continue;
        }
        for(int i=0;i<m&&i<temp.size();++i)
            printf("%s %d %d\n",temp[i].name,temp[i].age,temp[i].netWorth);
    }
    return 0;
}
```

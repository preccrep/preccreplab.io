---
layout: post
title: PAT 1105 Spiral Matrix
date: '2022-07-26 13:59:21 +0800'
categories: [刷题, PAT]
tags: [题解, 模拟, 数学]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805363117768704)

AC 代码:

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
#include <cmath>
using namespace std;
int N,m,n;
int main() {
    scanf("%d",&N);
    for(int i=sqrt(N);i>=1;--i)
        if(N%i==0){
            m=N/i,n=i;
            break;
        }
    vector<vector<int>>b(m+1);
    for(int i=1;i<=m;++i)
        b[i].resize(n+1);
    vector<int>a(N);
    for(int i=0;i<N;++i)
        scanf("%d",&a[i]);
    sort(a.begin(),a.end(),greater<int>());
    int index=0;
    int left=1,right=n,top=1,bottom=m;
    while(left<=right&&top<=bottom){
        for(int i=left;i<=right;++i)
            b[top][i]=a[index++];
        for(int i=top+1;i<bottom;++i)
            b[i][right]=a[index++];
        for(int i=right;i>=left;--i){
            if(top==bottom) break;
            b[bottom][i]=a[index++];
        }
        for(int i=bottom-1;i>top;--i){
            if(left==right) break;
            b[i][left]=a[index++];
        }
        left++,right--,top++,bottom--;
    }
    for(int i=1;i<=m;++i){
        for(int j=1;j<=n;++j){
            if(j!=1) printf(" ");
            printf("%d",b[i][j]);
        }
        printf("\n");
    }
    return 0;
}
```

---
layout: post
title: PAT 1050 String Subtraction
date: '2022-07-19 20:20:03 +0800'
categories: [刷题, PAT]
tags: [题解]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805429018673152)

题目很简单, 需要注意的有两点:
1. 使用 `getline(cin,str)` 之后, 如果接下来使用 `cin` 或 `scanf` 读入, 则需要 `getchar()` 来吞掉一个换行符; 如果使用 `getline(cin,str)` 之后继续使用 `getline(cin,str)`, 就不需要 `getchar()`;
2. ASCII 码的范围是 `0-127`, 不过后来发现这些字符不太能满足我们的需要, 所以就有了扩展 ASCII, 范围是 `0-255`, 正好占八位(一个字节). **所以范围是 `0-255` !**

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
using namespace std;
string s1,s2;
bool ascii[300];
int main() {
    getline(cin,s1);
    getline(cin,s2);
    for(int i=0;i<s2.size();++i)
        ascii[s2[i]]=true;
    for(int i=0;i<s1.size();++i)
        if(!ascii[s1[i]]) printf("%c",s1[i]);
    printf("\n");
    return 0;
}
```

---
layout: post
title: PAT 1167 Cartesian Tree
date: '2022-09-02 13:51:07 +0800'
categories: [刷题, PAT]
tags: [题解, 数据结构, 树]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/1478636026017230848)

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
#include <queue>
using namespace std;
int n;
queue<int>q;
struct node{
    int val,left=-1,right=-1;
}in[35];
int build(int left,int right){
    if(left>right) return -1;
    int root=left,minv=0x3f3f3f3f;
    for(int i=left;i<=right;++i)
        if(minv>in[i].val){
            minv=in[i].val;
            root=i;
        }
    in[root].left=build(left,root-1);
    in[root].right=build(root+1,right);
    return root;
}
int main() {
    scanf("%d",&n);
    for(int i=1;i<=n;++i)
        scanf("%d",&in[i].val);
    int root=build(1,n);
    q.push(root);
    while(!q.empty()){
        int top=q.front();
        q.pop();
        printf("%d",in[top].val);
        if(in[top].left!=-1) q.push(in[top].left);
        if(in[top].right!=-1) q.push(in[top].right);
        if(q.empty()) printf("\n");
        else printf(" ");
    }
    return 0;
}
```

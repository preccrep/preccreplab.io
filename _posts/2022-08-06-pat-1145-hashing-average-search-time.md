---
layout: post
title: PAT 1145 Hashing - Average Search Time
date: '2022-08-06 14:00:48 +0800'
categories: [刷题, PAT]
tags: [题解, 数据结构, 哈希]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805343236767744)

(正向的) 平方探测法.

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
#include <cmath>
using namespace std;
int len,n,m,a,num[100010];
bool isPrime(int x){
    if(x==1) return false;
    for(int i=2;i*i<=x;++i)
        if(x%i==0) return false;
    return true;
}
int main() {
    scanf("%d%d%d",&len,&n,&m);
    if(!isPrime(len))
        do{len++;}while(!isPrime(len));
    vector<int>b(len,0);
    for(int i=1;i<=n;++i){
        scanf("%d",&a);
        int j=0;
        while(j<len&&b[(a+j*j)%len]) j++;
        if(!b[(a+j*j)%len]) b[(a+j*j)%len]=a;
        else printf("%d cannot be inserted.\n",a);
    }
    double ans=0.0;
    for(int i=1;i<=m;++i){
        scanf("%d",&a);
        int j=0;
        while(j<=len&&b[(a+j*j)%len]!=a&&b[(a+j*j)%len]!=0) j++;
        ans+=j;
        if(j<=len) ans++;
    }
    ans/=m;
    printf("%.1lf\n",ans);
    return 0;
}
```

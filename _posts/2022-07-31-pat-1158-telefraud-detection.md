---
layout: post
title: PAT 1158 Telefraud Detection
date: '2022-07-31 15:53:44 +0800'
categories: [刷题, PAT]
tags: [题解, 并查集]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/1478635060278108160)

并查集一定要这样:

```c++
int fa1=findFather(x), fa2=findFather(y);
//对大小关系无要求
if(fa1!=fa2) fa[fa1]=fa2;
//对大小关系有要求
if(fa1>fa2) fa[fa1]=fa2;
else fa[fa2]=fa1;
//or
if(fa1!=fa2){
    if(fa1<fa2) swap(fa1,fa2);
    fa[fa1]=fa2;
}
```

AC 代码:

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
#include <map>
using namespace std;
const int N=1010;
int k,n,m;
int g[N][N];
vector<int>suspect;
map<int,vector<int>>ans;
int gang[N];
int findFather(int x){
    while(x!=gang[x]) x=gang[x]=gang[gang[x]];
    return x;
}
int main() {
    scanf("%d%d%d",&k,&n,&m);
    for(int i=1;i<=m;++i){
        int x,y,d;
        scanf("%d%d%d",&x,&y,&d);
        if(x!=y) g[x][y]+=d;
    }
    for(int i=1;i<=n;++i){
        int shortCall=0,callBack=0;
        for(int j=1;j<=n;++j){
            if(g[i][j]&&g[i][j]<=5){
                shortCall++;
                if(g[j][i]) callBack++;
            }
        }
        if(shortCall>k&&callBack*5<=shortCall)
            suspect.push_back(i);
    }
    if(suspect.empty()){
        printf("None\n");
        return 0;
    }
    for(int i=0;i<suspect.size();++i)
        gang[suspect[i]]=suspect[i];
    for(int i=0;i<suspect.size();++i)
        for(int j=i+1;j<suspect.size();++j){
            int x=suspect[i],y=suspect[j];
            if(g[x][y]&&g[y][x]){
                int fa1=findFather(x),fa2=findFather(y);
                if(fa1<fa2) gang[fa2]=fa1;
                else if(fa2<fa1) gang[fa1]=fa2;
            }
        }
    for(int i=0;i<suspect.size();++i){
        int fa=findFather(suspect[i]);
        ans[fa].push_back(suspect[i]);
    }
    for(auto &v:ans){
        for(int j=0;j<v.second.size();++j){
            if(j!=0) printf(" ");
            printf("%d",v.second[j]);
        }
        printf("\n");
    }
    return 0;
}
```

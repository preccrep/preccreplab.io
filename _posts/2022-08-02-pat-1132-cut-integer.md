---
layout: post
title: PAT 1132 Cut Integer
date: '2022-08-02 13:36:57 +0800'
categories: [刷题, PAT]
tags: [题解]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805347145859072)

Hmmmmmm 浮点错误什么原因大家心里都清楚.

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
#include <cmath>
using namespace std;
typedef long long ll;
int n;
ll z;
inline ll getLen(ll x){
    int cnt=0;
    while(x){
        x/=10;
        cnt++;
    }
    return (ll)pow(10,cnt>>1);
}
int main() {
    scanf("%d",&n);
    while(n--){
        scanf("%lld",&z);
        ll modLen=getLen(z);
        ll left=z/modLen,right=z%modLen;
        if(!left||!right) printf("No\n");
        else if(z%left==0&&(z/left)%right==0) printf("Yes\n");
        else printf("No\n");
    }
    return 0;
}
```

---
layout: post
title: PAT 1112 Stucked Keyboard
date: '2022-07-28 16:21:49 +0800'
categories: [刷题, PAT]
tags: [题解, 字符串, 模拟]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805357933608960)

找连续有 `k` 个相同字符的子串, 可以考虑这题中用 `pre` 记录前一个字符、用 `cnt%k==0` 判断的方法.

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
#include <unordered_set>
using namespace std;
int k;
char s[1010];
int v[40];
unordered_set<char>sett;
string stuck="",ans="";
inline int getIndex(char ch){
    return isdigit(ch)?ch-'0':(ch=='_'?37:ch-'a'+11);
}
int main() {
    scanf("%d%s",&k,s);
    char pre='#';
    int cnt=0;
    for(int i=0;i<strlen(s);++i){
        if(s[i]==pre) cnt++;
        else if(cnt%k==0){
            if(v[getIndex(pre)]!=-1) v[getIndex(pre)]=1;//目前可能是stucked
            cnt=1;
            pre=s[i];
        }else{
            v[getIndex(pre)]=-1;//一定不是stucked
            cnt=1;
            pre=s[i];
        }
    }
    if(cnt%k==0){
        if(v[getIndex(pre)]!=-1) v[getIndex(pre)]=1;
    }else v[getIndex(pre)]=-1;
    for(int i=0;i<strlen(s);){
        if(v[getIndex(s[i])]==1){
            if(!sett.count(s[i])){
                stuck+=s[i];
                sett.insert(s[i]);
            }
            ans+=s[i];
            i+=k;
        }else{
            ans+=s[i];
            i++;
        }
    }
    cout<<stuck<<endl<<ans<<endl;
    return 0;
}
```

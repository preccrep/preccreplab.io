---
layout: post
title: Repeatedly Being Asked to Install Command Line Tools
date: '2022-09-14 20:19:22 +0800'
categories: [Mac]
tags: [solutions]
---

After upgrading macOS to 12.6, I was unable to use `g++ -v`. When I run `g++ -v` in the command line, it repeatedly asked me to install command line tools. I tried to run `xcode-select --install`, but the option `--install` seemed to be wrong.

Later I found that the following command worked:

```bash
sudo xcode-select -switch /Library/Developer/CommandLineTools
```

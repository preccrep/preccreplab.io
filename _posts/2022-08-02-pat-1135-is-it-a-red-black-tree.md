---
layout: post
title: PAT 1135 Is It A Red-Black Tree
date: '2022-08-02 18:15:22 +0800'
categories: [刷题, PAT]
tags: [题解, 树, 红黑树]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805346063728640)

注意是检查每个节点的左子树和右子树中黑色节点的个数是否相等.

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
#include <unordered_set>
using namespace std;
int k,n;
int pre[35];
struct node{
    int val;
    struct node *left,*right;
};
node* Insert(node *root,int val){
    if(root==NULL){
        root=new node();
        root->val=val;
        root->left=root->right=NULL;
    }else if(abs(val)<=abs(root->val)) root->left=Insert(root->left,val);
    else root->right=Insert(root->right,val);
    return root;
}
bool judge1(node *root){
    if(root==NULL) return true;
    if(root->val<0){
        if(root->left!=NULL&&root->left->val<0) return false;
        if(root->right!=NULL&&root->right->val<0) return false;
    }
    return judge1(root->left)&&judge1(root->right);
}
int getBlack(node *root){
    if(root==NULL) return 0;
    int l=getBlack(root->left),r=getBlack(root->right);
    return max(l,r)+(root->val>0?1:0);
}
bool judge2(node *root){
    if(root==NULL) return true;
    int l=getBlack(root->left),r=getBlack(root->right);
    if(l!=r) return false;
    return judge2(root->left)&&judge2(root->right);
}
int main() {
    scanf("%d",&k);
    while(k--){
        scanf("%d",&n);
        node *root=NULL;
        for(int i=1;i<=n;++i){
            scanf("%d",&pre[i]);
            root=Insert(root,pre[i]);
        }
        if(pre[1]<0||!judge1(root)||!judge2(root)) printf("No\n");
        else printf("Yes\n");
    }
    return 0;
}
```

---
layout: post
title: PAT 1121 Damn Single
date: '2022-07-30 15:29:19 +0800'
categories: [刷题, PAT]
tags: [题解]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805352359378944)

不是有 CP 就可以无忧无虑了啊... ta 要是没来你也是孤身一人, just like a damn single 🐶.

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
using namespace std;
int n,x,y,m,cnt;
int couple[100010];
bool invited[100010];
vector<int>ans;
int main() {
    memset(couple,-1,sizeof(couple));
    scanf("%d",&n);
    for(int i=1;i<=n;++i){
        scanf("%d%d",&x,&y);
        couple[x]=y,couple[y]=x;
    }
    scanf("%d",&m);
    for(int i=1;i<=m;++i){
        scanf("%d",&x);
        invited[x]=true;
    }
    for(int i=0;i<100000;++i)
        if(invited[i])
            if(couple[i]==-1||!invited[couple[i]])
                ans.push_back(i);
    sort(ans.begin(),ans.end());
    printf("%d\n",ans.size());
    for(int i=0;i<ans.size();++i){
        if(i!=0) printf(" ");
        printf("%d",ans[i]);
    }
    return 0;
}
```

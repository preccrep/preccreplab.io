---
layout: post
title: PAT 1113 Integer Set Partition
date: '2022-07-28 17:08:26 +0800'
categories: [刷题, PAT]
tags: [题解, 贪心, 知识点]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805357258326016)

使用了 `<numeric>` 头文件中的 `accumulate` 函数, 可以简化求和步骤 (实则装 B).

用法:

```c++
//前两个形参指定要累加的元素范围，第三个形参是累加的初值.
accumulate ( 形参1 , 形参2 , 形参3 )

accumulate(a,a+n,1);
accumulate(v.begin(),v.end(),2);
accumulate(array,array+size(array),0);
```

AC 代码如下:

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
#include <numeric>
using namespace std;
typedef long long ll;
int n;
ll a[100010];
ll S1,S2;
int main() {
    scanf("%d",&n);
    for(int i=1;i<=n;++i)
        scanf("%lld",&a[i]);
    sort(a+1,a+1+n);
    printf("%d ",n&1);
    S1=accumulate(a+1,a+1+n/2,0);
    S2=accumulate(a+1+n/2,a+1+n,0);
    printf("%lld\n",abs(S1-S2));
    return 0;
}
```

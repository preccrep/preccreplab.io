---
layout: post
title: PAT 1040 Longest Symmetric String
date: '2022-07-20 14:09:53 +0800'
categories: [刷题, PAT]
tags: [题解, dp, palindrome]
---

[Link](https://pintia.cn/problem-sets/994805342720868352/problems/994805446102073344)

`dp[i][j]` 表示从 `s[i]` 到 `s[j]` 的子串是否是回文串, `dp[i][j] = 0 或 1`.

递推方程:
- if `s[i] == s[j]`:
    - `dp[i][j] = dp[i+1][j-1]`
- else:
    - `dp[i][j] = 0`

```c++
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <string.h>
#include <vector>
using namespace std;
string s;
int n,ans=1;
int dp[1010][1010];
int main() {
    getline(cin,s);
    n=s.size();
    for(int i=0;i<n;++i)
        dp[i][i]=1;
    for(int i=1;i<n;++i)
        if(s[i]==s[i-1]){
            dp[i-1][i]=1;
            ans=2;
        }
    for(int len=3;len<=n;++len)
        for(int i=0;i<n-len+1;++i){
            if(s[i]==s[i+len-1])
                dp[i][i+len-1]=dp[i+1][i+len-2];
            if(dp[i][i+len-1]) ans=len;
        }
    printf("%d\n",ans);
    return 0;
}
```

---
layout: post
title: 2023 Resolutions
date: '2022-07-31 12:44:18 +0800'
pin: true
---

> 2022年已经过去 7/12 了, 但是这一年剩下的时间并不能由我自由支配... 于是, 放到 2023 年做吧.

**TODO:**

- [ ] 学习 L2D !!!
- [ ] 做个游戏 (ue5?? 暂定)
- [ ] 折腾一下服务器...
- [ ] 应用上架


🏅 **Achievement Unlocked**: UPGRADE TO **SSC LV2** on `2022-08-06`{: .filepath}

🎖🎖 ENTER THE NEXT STAGE

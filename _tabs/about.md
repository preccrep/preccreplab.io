---
title: About
icon: fas fa-info-circle
order: 4
---

> Bubbles 是一名「[绘画病](https://ssc.moe/posts/%E7%BB%98%E7%94%BB%E7%97%85-%E7%9A%84%E7%A7%91%E6%99%AE/)」患者.
{: .prompt-info}

I'm `ssc`. You can also call me `ssc bubbles`, or 「 𝚜𝚜𝚌 🫧 」. I don't think we live in the same world... This is me:

![download20220803115551.png](https://s2.loli.net/2022/08/10/r71FPsvhqeEWiB5.png){: w="300" h="200" }

I have several identities. On weekdays I work for a game company, but on weekends I'm another person. Every night I will go out and play with my friends (so I don't really hate my job as a game developer).

I love drawing and reading and playing video games and I'm crazy about digital art.
